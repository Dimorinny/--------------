from django.core.management.base import BaseCommand
from optparse import make_option

from faker.frandom import random
from faker.lorem import sentence, sentences
from mixer.fakers import get_username, get_email, get_firstname, get_lastname
from mixer.generators import get_string, get_integer

import datetime
import csv
import MySQLdb

def writeCsv(matx, _fileName):
    fileName = "/var/www/ask_dim/main_app/csv/%s" %(_fileName)
    file_write = open(fileName, "w")
    wr = csv.writer(file_write)
    for line in matx:
        wr.writerow(line)
    file_write.close()

def generateDateTime():
	return datetime.datetime(get_integer(2010, 2014), get_integer(1, 11), get_integer(1, 27), get_integer(0, 23), get_integer(0, 59), get_integer(0, 59))

class DB:
	host = 'localhost'
	user = 'root'
	password = 'root'
	sqldb = 'askdb'
	conn = None

	def connect(self):
		self.conn = MySQLdb.connect(self.host,self.user,self.password,self.sqldb, local_infile = 1)

	def close(self):
		self.conn.close()

	def query(self, sql):
		cursor = self.conn.cursor()
		cursor.execute(sql)
		self.conn.commit()
		print vars(cursor)

	def load_data_infile(self, f, table):
		sql=r"""LOAD DATA LOCAL INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY ',' LINES TERMINATED BY '\r\n';""" % (f,table)
		self.query(sql)

class Command(BaseCommand):
	
	option_list = BaseCommand.option_list + (

		make_option(
			'--users',
			action = 'store',
			dest = 'users',
			default = 0,
		),

		make_option(
			'--questions',
			action = 'store',
			dest = 'questions',
			default = 0,
		),

		make_option(
			'--answers',
			action = 'store',
			dest = 'answers',
			default = 0,
		),

		make_option(
			'--tags',
			action = 'store',
			dest = 'tags',
			default = 0,
		),
	)

	def handle(*args, **options):

		usersList    = []
		profileList  = []
		questList    = []
		answerList   = []
		tagList      = []
		tagQuestList = []

		resultString = ""

		if options['users']:
			for i in xrange(1, int(options['users'])):
				usersList.append([i, "thisispassword", generateDateTime(), 0, get_username(), get_firstname(), get_lastname(), get_email(), 1, 1, generateDateTime()])
				profileList.append([i, i, get_integer(-250, 250), "/img/ava.jpg"])

			writeCsv(usersList, 'users.csv')
			writeCsv(profileList, 'profiles.csv')
			resultString += "Users - Ok "

		if options['questions'] and options['users']:
			for i in xrange(1, int(options['questions'])):
				questList.append([i, get_integer(1, len(profileList) - 1), sentence(), get_integer(-200, 200), sentences(get_integer(2,4)),  generateDateTime()])

			writeCsv(questList, 'questions.csv')
			resultString += "Questions - Ok "

		if options['questions'] and options['tags']:

			tags = ["Achievements", "activities", "algorithms", "analysis", "android", "article", "articles", "assignment", "bank", "birthday", "Books", "Brazil", "Bulls", "Campinas", "Career", "center", "combine", "Competitions", "control", "corporation", "cost", "covers", "data", "Data", "DBMS", "deduction", "Democracy", "designed", "development", "django", "DZ", "Eurozone", "exam", "financial", "frontend-regular", "group", "high", "HighLoad", "homework", "Homework", "hurray", "internship", "Interview", "introducing", "Java", "landmark", "largest", "leader", "lecture", "Lecture", "Linux", "Literature", "Mail.Ru", "meters", "Microsoft", "million", "mobile", "Molchanov", "nearly", "new", "opened", "placement", "poll", "portal", "possibility", "practices", "presentation", "private", "problem", "processing", "project", "Python", "ranking", "results", "Retake", "RK", "safe", "Santander", "schedule", "Scherbinin", "selection", "Seminars", "session", "Smal", "Spanish", "speed", "square", "stability", "structures", "Study", "system", "technologies", "technology", "Technopark", "testing", "third", "thousand", "tickets", "timetable", "training", "Trenin", "US", "Vacancy", "video", "Web", "web-technology", "work", "workshops"]

			for index, tag in enumerate(tags):
				tagList.append([index + 1, tag])

			currentQuest = 1

			for index in xrange(1, len(questList)):

				for counter in xrange(1, 3):
					bufferList = []
					random = get_integer(1, len(tags) - 1)

					if random not in bufferList:
						tagQuestList.append([currentQuest, index, random])
						bufferList.append(random)

					currentQuest += 1;


			writeCsv(tagList, 'tags.csv')
			resultString += "Tags - Ok "

			writeCsv(tagQuestList, 'tagquest.csv')
			resultString += "Tags quest - Ok"


		if options['questions'] and options['users'] and options['answers']:
			for i in xrange(0, int(options['answers'])):
				answerList.append([i, get_integer(1, len(profileList) - 1), get_integer(1, len(questList) - 1), sentence(), generateDateTime(), get_integer(0, 1)])

			writeCsv(answerList, 'answers.csv')
			resultString += "Answers - Ok "

		if options['questions'] and options['users'] and options['answers']:
			mysql = DB()
			mysql.connect()
			mysql.load_data_infile('/var/www/ask_dim/main_app/csv/users.csv', 'auth_user')
			mysql.load_data_infile('/var/www/ask_dim/main_app/csv/profiles.csv', 'main_app_profile')
			mysql.load_data_infile('/var/www/ask_dim/main_app/csv/questions.csv', 'main_app_question')
			mysql.load_data_infile('/var/www/ask_dim/main_app/csv/answers.csv', 'main_app_answer')
			mysql.load_data_infile('/var/www/ask_dim/main_app/csv/tags.csv', 'main_app_tag')
			mysql.load_data_infile('/var/www/ask_dim/main_app/csv/tagquest.csv', 'main_app_question_tags')


			mysql.close()
			resultString += "Write to db - Ok"

		return resultString
