from django.core.management.base import BaseCommand
from main_app.models import *
from django.contrib.auth.models import User
from optparse import make_option

class Command(BaseCommand):
    
    option_list = BaseCommand.option_list + (
        make_option(
            '--users',
            action = 'store',
            dest = 'users',
            default = 0,
        ),

        make_option(
            '--questions',
            action = 'store',
            dest = 'questions',
            default = 0,
        ),

        make_option(
            '--answers',
            action = 'store',
            dest = 'answers',
            default = 0,
        ),
    )
    
    
    def handle(*args, **options):
        if options['users']:
            writeCsv( [[]], "users.csv")




