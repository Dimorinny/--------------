from django.core.management.base import BaseCommand
from django.core.cache import caches
from main_app.models import Profile

memcached = caches['default']

class Command(BaseCommand):
	def handle(self, *args, **kwargs):
		top_profiles_model = Profile.objects.order_by("-rating")[:5]
		
		if top_profiles_model:
			top_profiles = []
			for profile in top_profiles_model:
				top_profiles.append( {"name": profile.user.username, "rating": profile.rating} )

		memcached.set('top_users', top_profiles)
		print memcached.get('top_users')



