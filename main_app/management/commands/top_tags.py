from django.core.management.base import BaseCommand
from django.core.cache import caches
from main_app.models import Tag
from django.db.models import Count

memcached = caches['default']

class Command(BaseCommand):
	def handle(self, *args, **kwargs):
		top_tags_model = Tag.objects.annotate(count_quest = Count('question')).order_by('-count_quest')[:25]
		
		if top_tags_model:
			top_tags = []
			for tag in top_tags_model:
				top_tags.append( {"name": tag.text } )

			memcached.set('top_tags', top_tags)
			print memcached.get('top_tags')