from django.db import models
from django.contrib.auth.models import User
from djangosphinx import SphinxSearch
import datetime



class Profile(models.Model):
	user       = models.OneToOneField(User)
	rating     = models.IntegerField(default = 0)
	avatar_url = models.CharField(max_length = 60)

class Tag(models.Model):
    text       = models.CharField(max_length = 255)

class Question(models.Model):
	author     = models.ForeignKey(Profile)
	title      = models.CharField(max_length = 150)
	rating     = models.IntegerField(default = 0)
	text       = models.TextField()
	tags       = models.ManyToManyField(Tag)
	date_added = models.DateTimeField(auto_now_add = True)
	search = SphinxSearch(
		weights = {
			'title': 100,
			'text': 90
		}
	)

class Answer(models.Model):
	author     = models.ForeignKey(Profile)
	question   = models.ForeignKey(Question)
	text       = models.TextField()
	date_added = models.DateTimeField(auto_now_add = True)
	is_right   = models.BooleanField(default = False)

class Like(models.Model):
	user     = models.ManyToManyField(User)
	question = models.ForeignKey(Question)
	value    = models.IntegerField(default = 0) 

