from django.conf.urls import include, url
from django.contrib import admin
import views



urlpatterns = [
    # Examples:
    # url(r'^$', 'ask_dim.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^signup/', views.signup),
    url(r'^login/', views.login),
    url(r'^question/', views.question),
    url(r'^user/', views.user_info),
    url(r'^method/login/', views.login_method),
    url(r'^method/logout/', views.logout_method),
    url(r'^method/question/create/', views.add_question_method),
    url(r'^method/answer/create/', views.add_answer_method),
    url(r'^method/answer/ok/', views.set_ok),
    url(r'^method/like/', views.like),
    url(r'^search/', views.search),
    url(r'^tag/', views.tags),
    url(r'^', views.index),
]
