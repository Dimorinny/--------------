# coding: utf8

from django import forms
from main_app.models import User, Profile
from ask_dim import settings

def handle_uploaded_file(f, id):
	filename = str(id) + '.' + f.name.split('.')[-1]

	path = settings.MEDIA_ROOT + filename
	destination = open(path, 'wb+')
	for chunk in f.chunks():
		destination.write(chunk)
	destination.close()
	return settings.MEDIA_URL + filename


class RegisterForm(forms.Form):
	userName   = forms.CharField(
		error_messages = {
			'required': 'Никнейм обязательное поле',
    		'invalid': 'Введите корректный никнейм'
		},
		widget = forms.TextInput(attrs = {
			'class': 'input_field login',
			'placeholder': 'Никнейм'
		}
	))

	firstName  = forms.CharField(
		error_messages = {
			'required': 'Имя - обязательное поле',
    		'invalid': 'Введите корректное имя'
		},
		widget = forms.TextInput(attrs = {
			'class': 'input_field login',
			'placeholder': 'Имя'
		}
	))

	secondName = forms.CharField(
		error_messages = {
			'required': 'Фамилия - обязательное поле',
    		'invalid': 'Введите корректную фамилию'
		},
		widget = forms.TextInput(attrs = {
			'class': 'input_field login',
			'placeholder': 'Фамилия'
		})
	)

	email = forms.EmailField(
		error_messages = {
			'required': 'Email - обязательное поле',
    		'invalid': 'Введите корректный email'
		},
		widget = forms.TextInput(attrs = {
			'class': 'input_field login',
			'placeholder': 'Email'
		})
	)

	password   = forms.CharField(
		error_messages = {
			'required': 'Пароль - обязательное поле',
    		'invalid': 'Введите корректный пароль'
		},
		widget = forms.PasswordInput(attrs = {
			'class': 'input_field login',
			'placeholder': 'Пароль'
		})
	)

	ava = forms.FileField(
		error_messages = {
			'required': 'Аватарка - обязательное поле',
		},
		widget = forms.FileInput(attrs = {
			'onchange': 'readUrl(this);'
		})
	)

class UserForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
	    super(UserForm, self).__init__(*args, **kwargs)

	    if 'password' in self.fields:
	        self.fields['password'].required = False

	ava = forms.FileField(
		required=False,
		widget = forms.FileInput(attrs = {
		'onchange': 'readUrl(this);'
		})
	)

	class Meta:
		model = User
		fields = ['username', 'email', 'first_name', 'last_name', 'password']

		widgets = {

			'username': forms.TextInput(attrs = {
				'class': 'input_field login',
			}),

			'email': forms.TextInput(attrs = {
				'class': 'input_field login',
			}),

			'first_name': forms.TextInput(attrs = {
				'class': 'input_field login',
			}),

			'last_name': forms.TextInput(attrs = {
				'class': 'input_field login',
			}),

			'password': forms.PasswordInput(attrs = {
				'class': 'input_field login'
			})
		}


	def save(self, fileRequest = False, commit = True):
		self.instance.set_password(self.cleaned_data['password'])

		if commit:
			self.instance.save()
			if fileRequest:
				self.instance.profile.avatar_url = handle_uploaded_file(fileRequest, self.instance.id)
			self.instance.profile.save()

		return self.instance


