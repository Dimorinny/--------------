from django.contrib import admin
from main_app.models import Question, Answer

class QuestionAdmin(admin.ModelAdmin):
	fields = ['title', 'author', 'text']

admin.site.register(Question, QuestionAdmin)

# Register your models here.
