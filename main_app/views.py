# coding: utf8

from django.shortcuts import render, render_to_response, redirect
from django.http import HttpRequest, HttpResponse

from main_app.models import Profile, Question, Answer, Tag, Like
from main_app.forms  import RegisterForm, UserForm
from ask_dim import settings

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.core.context_processors import csrf
from django.template import RequestContext

from django.contrib import auth
from django.contrib.auth.models import User
from django.views.decorators.http import require_POST

from django.db import IntegrityError

from datetime import datetime

import json

def handle_uploaded_file(f, id):
	filename = str(id) + '.' + f.name.split('.')[-1]

	path = settings.MEDIA_ROOT + filename
	
	with open(path, 'wb+') as destination:
		for chunk in f.chunks():
			destination.write(chunk)

	return settings.MEDIA_URL + filename

def index(request):
	args = {}
	args.update(csrf(request))

	posts_in_page = 5
	user          = auth.get_user(request)
	page_number   = request.GET.get('page')
	sorted_by     = request.GET.get('sorted-by')

	if sorted_by and sorted_by == "rating":
		questions = Question.objects.all().order_by('-rating')
	else:
		questions = Question.objects.all().order_by('-date_added')

	paginator = Paginator(questions, posts_in_page)

	try:
		list_questions = paginator.page(page_number).object_list

	except PageNotAnInteger:
		list_questions = paginator.page(1).object_list
		page_number = 1

	except EmptyPage:
		list_questions = paginator.page(paginator.num_pages).object_list

	args['user'] = user
	args['page'] = page_number
	args['list_questions'] = list_questions
	args['col_posts'] = questions.count
	args['posts_in_page'] = posts_in_page

	return render_to_response("index.html", args)
	
def question(request):
	args = {}
	args.update(csrf(request))

	comments_in_page = 6
	quest_id = request.GET.get('id')
	page_number = request.GET.get('page')
	user = auth.get_user(request)

	if quest_id and quest_id.isdigit() and int(quest_id) <= Question.objects.all().count() and int(quest_id) > 0:
		question = Question.objects.get(id=quest_id)

		answers = question.answer_set.all().order_by('-date_added')

		paginator = Paginator(answers, comments_in_page)

		try:
			list_answers = paginator.page(page_number).object_list

		except PageNotAnInteger:
			list_answers = paginator.page(1).object_list
			page_number = 1

		except EmptyPage:
			list_answers = paginator.page(paginator.num_pages).object_list

		args['has_right'] = answers.filter(is_right=1).count() > 0
		args['page'] = page_number
		args['quest_id'] = quest_id
		args['list_answers'] = list_answers
		args['question'] = question
		args['answers_col'] = answers.count
		args['answers_in_page'] = comments_in_page
		args['user'] = user

		return render_to_response("question-page.html", args)

	else:
		return redirect('/')

def signup(request):

	if auth.get_user(request).is_authenticated():
		return redirect('/')

	args = {}
	args.update(csrf(request))

	if request.method == 'POST':
		regForm = RegisterForm(request.POST, request.FILES)

		if regForm.is_valid():

			try:
				user = User()
				user.username = regForm.cleaned_data['userName']
				user.email = regForm.cleaned_data['email']
				user.first_name = regForm.cleaned_data['firstName']
				user.last_name = regForm.cleaned_data['secondName']
				user.set_password(regForm.cleaned_data['password'])
				user.save()

				profile = Profile()
				profile.rating = 0
				profile.user = user
				profile.avatar_url = handle_uploaded_file(request.FILES['ava'], user.id)
				profile.save()

			except IntegrityError:
				args['form'] = regForm;
				return render_to_response("signup.html", args)

			user = auth.authenticate(username = regForm.cleaned_data['userName'], password = regForm.cleaned_data['password'])
			auth.login(request, user)
			return redirect('/')

		else:
			args['form'] = regForm;
			return render_to_response("signup.html", args)

	else:
		args['form'] = RegisterForm()
		return render_to_response("signup.html", args)


def login(request):

	if auth.get_user(request).is_authenticated():
		return redirect('/')

	args = {}
	args.update(csrf(request))
	return render_to_response("login.html", args, context_instance = RequestContext(request))


# Структура ответа:
# Success - Все окей, авторизация успешна
# Invalid Username or password - Просто, не правильно
# User is not active - Данный юзер не активный

@require_POST
def login_method(request):
	args = {}
	args.update(csrf(request))

	resultJson = {}

	username = request.POST.get('login', '')
	password = request.POST.get('password', '')

	user = auth.authenticate(username=username, password=password)

	if user:
		if user.is_active:
			auth.login(request, user)

			resultJson['status'] = 'ok'
			resultJson['message'] = 'Success'
			return HttpResponse(json.dumps(resultJson), content_type="application/json")
		
		else:
			resultJson['status'] = 'not ok'
			resultJson['message'] = 'Пользователь не активирован'
			return HttpResponse(json.dumps(resultJson), content_type="application/json")
	else:
		resultJson['status'] = 'not ok'
		resultJson['message'] = 'Неправильно введен логин или пароль'
		return HttpResponse(json.dumps(resultJson), content_type="application/json")

@require_POST
def add_question_method(request):

	resultJson = {}

	title = request.POST.get('title', '')
	text  = request.POST.get('text', '')
	tags  = request.POST.get('tags', '')

	tagsArray = tags.split(',')

	user  = auth.get_user(request)

	if title and text:

		if user.is_authenticated():
				
			question = Question()
			question.text = text
			question.title = title
			question.rating = 0
			question.date_added = datetime.now()
			question.author = user.profile

			question.save()

			if len(tagsArray) and tagsArray[0]:
				tagsObjectsArr = []
				for tag_text in tagsArray:
					tag = Tag.objects.get_or_create(text=tag_text)
					question.tags.add(tag[0])

			resultJson['status'] = 'ok'
			resultJson['info'] = 'question added'
			resultJson['tags'] = tagsArray
			return HttpResponse(json.dumps(resultJson), content_type="application/json")
	
		else:
			resultJson['status'] = 'not posting'
			resultJson['info'] = 'auth error'
			return HttpResponse(json.dumps(resultJson), content_type="application/json")

	else:
		resultJson['status'] = 'not posting'
		resultJson['info'] = 'title or text is empty'
		return HttpResponse(json.dumps(resultJson), content_type="application/json")

@require_POST
def add_answer_method(request):

	resultJson = {}

	idQuest = request.POST.get('id_quest', '')
	textAnswer = request.POST.get('text_answer', '')

	user  = auth.get_user(request)

	if idQuest and textAnswer and idQuest.isdigit() and Question.objects.count() <= idQuest:

		if user.is_authenticated():
			answer = Answer()
			answer.author = user.profile
			answer.question_id = idQuest
			answer.text = textAnswer
			answer.date_added = datetime.now()
			answer.is_right = 0
			answer.save()

			resultJson['status'] = 'ok'
			resultJson['info'] = 'Answer added'
			resultJson['id'] = idQuest
			return HttpResponse(json.dumps(resultJson), content_type="application/json")

		else:
			resultJson['status'] = 'error'
			resultJson['info'] = 'error auth'
			return HttpResponse(json.dumps(resultJson), content_type="application/json")

	else:
		resultJson['status'] = 'error'
		resultJson['info'] = 'format answer error'
		return HttpResponse(json.dumps(resultJson), content_type="application/json")


def logout_method(request):
	auth.logout(request)
	return redirect('/')

@require_POST
def set_ok(request):
	args = {}
	args.update(csrf(request))
	
	resultJson = {}

	resultJson['status'] = 'fail'
	resultJson['do'] = 'none'

	idAnswer = request.POST.get('id', '')
	ok_command = request.POST.get('ok', '')

	if idAnswer and idAnswer.isdigit():
		answer = Answer.objects.get(id = idAnswer)
		if answer:

			if ok_command == "1":
				answer.is_right = 1
				answer.save()
				resultJson['status'] = 'ok'
				resultJson['do'] = 'liked'
				resultJson['id'] = answer.question_id
			else:
				answer.is_right = 0
				answer.save()
				resultJson['status'] = 'ok'
				resultJson['do'] = 'disliked'
				resultJson['id'] = answer.question_id

	return HttpResponse(json.dumps(resultJson), content_type="application/json")


@require_POST
def like(request):
	args = {}
	args.update(csrf(request))

	try:
		questId = int( request.POST.get('questId', '') )
		data = int( request.POST.get('dataId', '') )

		resultJson = {}
		curr_user = auth.get_user(request)

		if curr_user.is_authenticated():
			quest = Question.objects.get(id=questId)
			like  = Like.objects.get(question=quest.id, user=curr_user.id)


			resultJson['status'] = 'fail'
			resultJson['info'] = 'Already liked'
					
		else:
			resultJson['status'] = 'fail'
			resultJson['info'] = 'Auth error'

	except ValueError:
		resultJson['status'] = 'fail'
		resultJson['info'] = 'Info format error'

	except Like.DoesNotExist:
		like = Like()
		like.question = quest
		like.value = data
		like.save()
		like.user.add(curr_user)
		like.save()
		quest.rating = quest.rating + data
		quest.save()

		resultJson['status'] = 'Ok'
		resultJson['new_rating'] = quest.rating

	return HttpResponse(json.dumps(resultJson), content_type="application/json")


def user_info(request):
	args = {}
	args.update(csrf(request))

	user = auth.get_user(request)

	if user.is_authenticated():

		if request.method == 'POST':
			userForm = UserForm(request.POST, request.FILES, instance = user)
			
			if userForm.is_valid():

				if 'ava' in request.FILES:
					userForm.save(request.FILES['ava'])
				else:
					userForm.save()

		else:
			userForm = UserForm(instance = user)

	else:
		return redirect('/')

	args['user_form'] = userForm
	args['user'] = user
	return render_to_response("user.html", args)

def search(request):
	args = {}
	query = request.GET.get("query", "")
	page_number   = request.GET.get('page')
	args.update(csrf(request))

	posts_in_page = 5
	user          = auth.get_user(request)

	if not query:
		return redirect("/")

	questions = Question.search.query(unicode(query))
	paginator = Paginator(questions, posts_in_page)

	try:
		list_questions = paginator.page(page_number).object_list

	except PageNotAnInteger:
		list_questions = paginator.page(1).object_list
		page_number = 1

	except EmptyPage:
		list_questions = paginator.page(paginator.num_pages).object_list

	args['query'] = query
	args['user'] = user
	args['page'] = page_number
	args['list_questions'] = list_questions
	args['col_posts'] = questions.count
	args['posts_in_page'] = posts_in_page

	return render_to_response("search.html", args)

def user_info(request):
	args = {}
	args.update(csrf(request))

	user = auth.get_user(request)

	if user.is_authenticated():

		if request.method == 'POST':
			userForm = UserForm(request.POST, request.FILES, instance = user)
			
			if userForm.is_valid():

				if 'ava' in request.FILES:
					userForm.save(request.FILES['ava'])
				else:
					userForm.save()

		else:
			userForm = UserForm(instance = user)

	else:
		return redirect('/')

	args['user_form'] = userForm
	args['user'] = user
	return render_to_response("user.html", args)

def tags(request):
	args = {}
	query = request.GET.get("search", "")
	page_number = request.GET.get('page')
	args.update(csrf(request))

	posts_in_page = 5
	user          = auth.get_user(request)

	tag = Tag.objects.get(text=query)

	if not query or not tag:
		return redirect("/")

	questions = Question.objects.filter(tags__in=[tag]).order_by("-date_added")
	paginator = Paginator(questions, posts_in_page)

	try:
		list_questions = paginator.page(page_number).object_list

	except PageNotAnInteger:
		list_questions = paginator.page(1).object_list
		page_number = 1

	except EmptyPage:
		list_questions = paginator.page(paginator.num_pages).object_list

	args['query'] = query
	args['user'] = user
	args['page'] = page_number
	args['list_questions'] = list_questions
	args['col_posts'] = questions.count
	args['posts_in_page'] = posts_in_page

	return render_to_response("search.html", args)


