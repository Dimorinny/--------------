#encoding: utf8
from django import template
from django.core.cache import caches

memcached = caches['default']

register = template.Library()

@register.inclusion_tag('tags/top_users.html')
def bestUsers():
	return {'users': memcached.get("top_users") }

@register.inclusion_tag('tags/top_tags.html')
def bestTags():
	return {'tags': memcached.get("top_tags") }
