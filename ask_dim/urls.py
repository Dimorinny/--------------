from django.conf.urls import include, url
from django.contrib import admin
import debug_toolbar
from django.conf import settings


urlpatterns = [
    # Examples:
    # url(r'^$', 'ask_dim.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),    
    url(r'^__debug__/', include(debug_toolbar.urls)),
    url(r'^', include('main_app.urls'))
]

