# coding=utf-8
from cgi import parse_qs, escape

def application(environ, start_response):
    status = '200 OK'
    output = '''
    <meta charset="utf8">
    Hello world! 
    <br> GET параметры:
    '''
   
    GET = parse_qs(str(environ.get('QUERY_STRING')))

    for key in GET:
        for sub_key in GET[key]:
            output += key + " => " + "'" + sub_key + "'" + " "

    output += "<br> POST параметры: "

    try:
        request_body_size = int(environ.get('CONTENT_LENGTH', 0))
    except (ValueError):
        request_body_size = 0

    request_body = environ['wsgi.input'].read(request_body_size)
    query_list = parse_qs(request_body)

    for key in query_list:
        for sub_key in query_list[key]:
            output += key + " => " + "'" + sub_key + "'" + " "

    response_headers = [('Content-type', 'text/html'), ("Content-length", str(len(output)))]
    start_response(status, response_headers)
    	
    return [output]
