import os
import sys
    
path = '/var/www/ask_dim/'
sys.path.append(path)

os.environ['DJANGO_SETTINGS_MODULE'] = 'ask_dim.settings'

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
